#!/bin/bash
mkdir -p ~/.env
ln -sirT zshrc.local ~/.zshrc.local
ln -sirT zsh ~/.zsh
ln -sir environ-homebin ~/.env/zsh-homebin.env
